# Lab 2 -- Linter and SonarQube as a part of quality gates

[![pipeline status](https://gitlab.com/desmigor/s23-lab2-linter-and-sonar-as-a-part-of-qg/badges/master/pipeline.svg)](https://gitlab.com/desmigor/s23-lab2-linter-and-sonar-as-a-part-of-qg/-/commits/master)

## Part 1: SonarQube

1. I run the SonarQube in a docker container using the command:

    ```
    docker run -d --name sonarqube -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true -p 9000:9000 sonarqube:latest
    ```
2. Logged in and created a new project manually.

3. I run the analysis using the following command generated after creating the project:

    ```
    ./mvnw clean verify sonar:sonar \
      -Dsonar.projectKey=lab2_igor \
      -Dsonar.host.url=http://localhost:9000 \
      -Dsonar.login=sqp_a286ea8dc89b9edfef0e928591f11f3463404ea8
    ```

4. Here're the results from SonarQube analysis:

    ![](./images/sonarqube.png)


## Part 2: SonarCloud

Sonarcloud Results: 

![](./images/sonarcloud.png)